﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class LaserGun : NetworkBehaviour
{

    public Transform laserTransform;
    public LineRenderer line;


   
    void Start()
    {
        
    }

    
    void Update()
    {
        if (isLocalPlayer && Input.GetMouseButton(0))
        {
            CmdShoot();
        }
    }


    [Command]
    public void CmdShoot()
    {
        Ray ray = new Ray(laserTransform.position, laserTransform.forward);

        if(Physics.Raycast(ray, out RaycastHit hit , 100f))
        {
            var player = hit.collider.gameObject.GetComponent<LaserGun>();

            if (player)
            {
                StartCoroutine(Respawn(hit.collider.gameObject));

            }
            RpcDrawLaser(laserTransform.position, hit.point);
        }
        else
        {
            RpcDrawLaser(laserTransform.position, laserTransform.position + laserTransform.forward * 100f);
        }
    }


    [Server]
    IEnumerator Respawn(GameObject go)
    {
        NetworkServer.UnSpawn(go);
        Transform newPos = NetworkManager.singleton.GetStartPosition();
        go.transform.position = newPos.position;
        go.transform.rotation = newPos.rotation;

        yield return new WaitForSeconds(1f);
        NetworkServer.Spawn(go, go);
    }


    [ClientRpc]
    void RpcDrawLaser(Vector3 start, Vector3 end)
    {
        StartCoroutine(LaserFlash(start, end));
    }

    IEnumerator LaserFlash(Vector3 start, Vector3 end)
    {
        line.SetPosition(0, start);
        line.SetPosition(1, end);

        yield return new WaitForSeconds(0.3f);

        line.SetPosition(0, Vector3.zero);
        line.SetPosition(1, Vector3.zero);
    }
}
